# IPD-440_Regularization_for_Deep_Learning

Presentación y códigos utilizadas en la clase de IPD-440 S2-2021 que trata sobre tipos de regularización en maquinas de aprendizaje.
Autores: Patricio Carrasco y Gustavo Silva

## Existentes
Se encuentra dentro una presentacion y 3 ejemplos de codigo, uno sobre normalizacion y droput, otro de Early Stopping y finalmente de Data Augment.
Los codigos son libres de ocuparse y de modificarse. Ante cualquier duda comunicarse con mediante correo electronico a:	gustavo.silvasi@sansano.usm.cl


